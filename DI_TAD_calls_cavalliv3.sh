#! /bin/bash
#
python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/ES/ES_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 50 301 263909 1 20 10000 1 0 False ES_Cav_DI50

python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/NPC/NPC_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 50 301 263909 1 20 10000 1 0 False NPC_Cav_DI50

python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/CN/CN_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 50 301 263909 1 20 10000 1 0 False NP_Cav_DI50

#python DI_TAD_calls2.py /project/jcreminslab/Data/project012_BlobelBrd2HiC/ICED_40kb_7_10_2016/hic_results/matrix/KO_D_MergedReps/iced/40000/KO_D_MergedReps_40000_iced.matrix /project/jcreminslab/Data/project012_BlobelBrd2HiC/output_Hsu_HiC_allmergedfiles_07_02_2016/hic_results/matrix/KO_C8_MergedReps/raw/40000/KO_C8_MergedReps_40000_abs.bed 50 76 66382 1 21 40000 2 0 False KO_D_2
