#! /bin/bash
#


bsub -o ESC4DN.o -e ESC4DN.e python DI_TAD_calls2Hermitianmodv2.py  input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.counts input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.bed 50 79 386046 1 24 8000 1 0 False 2 ESC4DN_merge_DI50adjust2

bsub -o ESC4DN.o -e ESC4DN.e python DI_TAD_calls2Hermitianmodv2.py  input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.counts input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.bed 50 79 386046 1 24 8000 1 0 False 1 ESC4DN_merge_DI50

bsub -o ESC4DN.o -e ESC4DN.e python DI_TAD_calls2Hermitianmodv2.py  input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.counts input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.bed 75 79 386046 1 24 8000 1 0 False 1 ESC4DN_merge_DI75

bsub -o ESC4DN.o -e ESC4DN.e python DI_TAD_calls2Hermitianmodv2.py  input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.counts input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.bed 100 79 386046 1 24 8000 1 0 False 1 ESC4DN_merge_DI100

bsub -o ESC4DN.o -e ESC4DN.e python DI_TAD_calls2Hermitianmodv2.py  input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.counts input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.bed 150 79 386046 1 24 8000 1 0 False 1 ESC4DN_merge_DI150

bsub -o ESC4DN.o -e ESC4DN.e python DI_TAD_calls2Hermitianmodv2.py  input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.counts input/U54-ESC4DN-FA-DpnII-2017524-R1-R2.1000_hg38.multires.balanced_genomewide_8kb.bed 250 79 386046 1 24 8000 1 0 False 1 ESC4DN_merge_DI250


