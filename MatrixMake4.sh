#! /bin/bash
#
# Script file to make lots of batches of MatrixMake Outputs

#make HeatMap

for i in {64..66}
do
    n=$i
    if [ $n -eq 1 ]
    then
        start=$(($n*76))
    else
        start=$((76+$(($n-1))*1000-300))
    fi

    if [ $n -eq 66 ]
    then
        end=66382
    else
        end=$((1000*$n))
    fi
    
    echo "$start"
    echo "$end"
    python MatrixMake4.py /project/jcreminslab/Data/project012_BlobelBrd2HiC/ICED_40kb_7_10_2016/hic_results/matrix/KO_D_MergedReps/iced/40000/KO_D_MergedReps_40000_iced.matrix  /project/jcreminslab/Data/project012_BlobelBrd2HiC/output_Hsu_HiC_allmergedfiles_07_02_2016/hic_results/matrix/KO_C8_MergedReps/raw/40000/KO_C8_MergedReps_40000_abs.bed $start $end $n testv

done

#DI on each HeatMap

dir=/project/jcreminslab/emersond_projects/clustering/heatmaps2/KO_D*_heatmap.csv
i=1
for file in $dir
do
    python DI.5.py  $file 50 $i

    i=$(($i+1))
done

#Stitch DIs and remove overlap

dir=/project/jcreminslab/emersond_projects/clustering/heatmaps2/DI*.csv
i=1
for file in $dir
do
     echo "$file"
     awk -F',' '{for (i = 126;i<= NF;i++) print $i}' $file > DI$i\chop.txt
     i=$(($i+1))
done

cat DI1chop.txt DI2chop.txt > total2chop.txt
#totalDI=/project/jcreminslab/emersond_projects/clustering/heatmaps2/total2chop.txt
for i in {3..66}
do
    echo "$i"
    before=$(($i-1))
    cat total$before\chop.txt DI$i\chop.txt > total$i\chop.txt
done


