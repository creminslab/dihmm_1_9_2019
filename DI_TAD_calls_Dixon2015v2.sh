#! /bin/bash
#

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 1 77403 1 24 40000 1 0 False dixon2015humanESDI50

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 100 58 77403 1 24 40000 1 0 False dixon2015humanESDI100

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 500 1 77403 1 24 40000 1 0  False dixon2015humanESrawDI500

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 1 77403 1 24 40000 1 0  False dixon2015humanESexprawDI50  #attempted, failed  Numerical result out of range

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 1 77403 1 24 40000 1 0   False dixon2015humanESexpicedDI50  #attempted, failed  Numerical result out of range

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 1 77403 1 24 40000 1 0  False  dixon2015humanESsqrtrawDI50
#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 20 1 77403 1 24 40000 1 0 False  dixon2015humanESsqrtrawDI20

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 35 1 77403 1 24 40000 1 0 False  dixon2015humanESsqrtrawDI35

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 10 1 77403 1 24 40000 1 0  False  dixon2015humanESsqrtrawDI10

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 15 1 77403 1 24 40000 1 0 False  dixon2015humanESsqrtrawDI15

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 10 1 77403 1 24 40000 1 0 False  dixon2015humanESsqrtrawDI10

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 8 1 77403 1 24 40000 1 0 False  dixon2015humanESsqrtrawDI8

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 7 1 77403 1 24 40000 1 0 False      dixon2015humanESsqrtrawDI7

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 6 1 77403 1 24 40000 1 0 False      dixon2015humanESsqrtrawDI6

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 5 1 77403 1 24 40000 1 0 False      dixon2015humanESsqrtrawDI5

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 7 1 77403 1 24 40000 1 0 False  dixon2015humanESlograwDI7

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 6 1 77403 1 24 40000 1 0 False  dixon2015humanESlograwDI6

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 5 1 77403 1 24 40000 1 0 False  dixon2015humanESlograwDI5

#python DI_TAD_calls2Hermitian.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 58 77403 1 24 40000 1 0 False  dixon2015humanESlogicedDI50

#python DI_TAD_calls2Hermitianmod.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 58 77403 1 24 40000 1 0 False  dixon2015humanESicedmodDI50

#python DI_TAD_calls2Hermitianmod.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 58 77403 1 24 40000 1 0 False  dixon2015humanESrawmodDI50

#python DI_TAD_calls2Hermitianmod.py input/Dixon2015_humanES_HiC_MERGED_40000.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 1 77403 1 24 40000 1 0 False  dixon2015humanESrawmodv2DI50

#python DI_TAD_calls2Hermitianmod.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 50 1 77403 1 24 40000 1 0 False dixon2015humanESicedmodv2DI50

#python DI_TAD_calls2Hermitianmod.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 10 1 77403 1 24 40000 1 0 False dixon2015humanESicedmodv2DI10

#python DI_TAD_calls2Hermitianmod.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 20 1 77403 1 24 40000 1     0 False dixon2015humanESicedmodv2DI20

python DI_TAD_calls2Hermitianmod.py input/Dixon2015_humanES_HiC_MERGED_40000_iced.matrix input/Dixon2015_humanES_HiC_MERGED_40000_abs.bed 35 1 77403 1 24 40000 1 0 False dixon2015humanESicedmodv2DI35
