**Introduction**
================
|
| DIHMM is a package for determining topological domains based on Directionality Index combined with mixed Gaussian Hidden Markov Model
(original paper: https://www.nature.com/articles/nature11082)
 
 
| 
**Overview**
=============
|
| place input counts and bed file in input directory
output TADboundaries and meta files placed in output directory

1) run DI_TAD_calls2.sh

2) go to postprocess directory and run command:
   ::

      awk -F'\t' '{printf("%d\t%d\t%d\t%f\t%d\t%d\t%f\t%f\t%f\n", $1,$2,$3,$4,$5,$6,$7,$8,$9)}' ../output/(name)_postp_list.csv | perl converter_7col.pl > ../output/(name)_hmm_7colfile_c 

3) run the final postprocess bash script in postprocess directory:
   finalprocess.sh





|
**Setup**
=========
|
To run the faster c code version be sure to compile fwdback2.c and hmmFwdBack.c into shared objects:
    ::

      gcc -std=c99 fwdback2.cc  -fPIC -shared -o _fwdback2.so, gcc -std=c99 hmmFwdBack.cc  -fPIC -shared -o _hmmFwdBack.so)

The wrappers fwdback2c.py and hmmFwdBackc.py are used to pass in  numpy arrays into shared objects

|
**Run DI_TAD_calls2.sh**
========================
|
|
input
-----
|
| DI_TAD_calls2.sh is composed of:
| Arg 1: count file
| Arg 2: bed file
| Arg 3: number of DI bins upstream and downstream (Dixon et al suggest 2 MB - corresponding to 50 with binsize of 40 KB)
| Arg 4: first index in count file
| Arg 5: last index in count file (if running whole genome)
| Arg 6: first chromosome
| Arg 7: last chromosome (running whole genome)
| Arg 8: binsize number of bases
| Arg 9: window size (default is 1, DI being calculated across number of diagonal bins)
       (e.g. if binsize is 40KB, user can modify window to 2. Thus, DI will be calculated over 80 KB region) 
| Arg 10: gap between DI upstream down stream and diagonal (Default is 0):
| Arg 11: False (uses bandedmatrix to calculate DI - much faster)
| Arg 12: name for output (name)_postp_list.csv (located in output directory)


|
output
------
|
| The output (name)_postp_list.csv is composed of 9 columns (placed in output/ subdirectory):
| col1: chromosome #
| col2: start of base region
| col3: end of base region
| col4: DI index
| col5: viterbi path (best of 20 different clusterings (1-20 mixturecomponents))
| col6: Baum-Welch path (best of 20 different clusterings (1-20mixturecomponents))
| col7: probability of state 1 in best of 20
| col8: probability of state 2 in best of 20
| col9: probability of state 3 in best of 20

| Best of 20 is decided by aic (Dixon et al), basically it is determined by the final logliklihood and number of points.See DI_TAD_calls2.py for code



|
| **##Notes##**
|
| The initial step first makes the heatmap (nonsquare) using MatrixMakeBanded4.py which then runs DI8.py to calculate DIs.  Then the bad DIs (bleed through of DI at end and beginning of chromosomes where index is influenced by previous chromosome bins are removed and matched with  bed for postprocessing.  Finally, the finalized DIs (col 4 of new array) is then fed into HMM .  3 hidden states (upstream, no bias, downstream) with mixture cluster range 1-20 (Dixon et al setting) is then used.  The Baum Welch pathway and Viterbi pathway are calculated


|
**Postprocess**
===============
|
| after (name)_postp_list.csv is created, run in postprocess directory:
   ::

      awk -F'\t' '{printf("%d\t%d\t%d\t%f\t%d\t%d\t%f\t%f\t%f\n", $1,$2,$3,$4,$5,$6,$7,$8,$9)}' ../output/(name)_postp_list.csv | perl converter_7col.pl > ../output/(name)_hmm_7colfile_c

This ensures that hidden states 1-3 correspond correctly to upstream (lowest DI<0), DI ~ 0, and downstream (highest DI > 0) respectively

|
| create genome_sequence.txt in output directory.  Should have 2 columns:(chromosome numbering MUST be numeric, convert to number otherwise. Note: the original bedfile chromosomes can have non-numeric value because convertbed.py converts to a number):

    ::

      chr1     number of bases in chr1
      chr2     number of bases in chr2
      chr3     number of bases in chr3 
      ...      .......................
      ...      .......................
      chr21    number of bases in chr21

|
| run finalprocess.sh:
|
| edit top 4 numbers (start chr, stop chr, binsize, and window size)
|
| uses (name)_hmm_7colfile_c  output from step 2
| keep default settings (Dixon et al.) 2 bins - designates cutoff for minimum TADsize
| uses 0.99 - 99% certainty that bin belongs to certain hidden state
|
| Perl scripts designate a TAD as having both an upstream (state 3) and downstream (state 1) component

|
|
**Note Bene**
=============
|
| look at http://www.cs.ubc.ca/~murphyk/Papers/learncg.pdf for Kevin Murphy's original paper with equations
|
| It is assumed that .bed file is organized genome wide with the indices running in consecutive sequence such that the start index of the next chr is 1 larger than last index of previous chr.  Also, it is assumed that successive chromosome start pcounts at same base position as chr1 (more conservative, smaller TAD region consideration, and more false negatives), which begins at user specified index.  Checkout the threshold setting in convertbed2.py.  If chr2 etc. are assumed to not measure from same base position as chr 1 (i.e. measure from 0), comment the current threshold (starti + DIrange + distance - 1)*binsize and choose (DIrange + distance)*binsize.  This will increase TAD region consideration for successive chromosomes (less conservative).
|  
| I switched to new simpler script MatrixMakeBanded4.py.  Makes numpy array of zeros and fills in values rather than starting with values and filling zeros gaps.
|
| Unit testing for HMM done with HMM_test.py.  Input/output for the HMM functions were compared with MATLAB HMM (Kevin Murphy).
