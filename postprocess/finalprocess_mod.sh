#! /bin/bash
# inputfile must be bed file plus corresponding DI and hidden state values

#Quis custodiet ipsos custodes?


value0=1 #start chr
value1=24 #stop chr
#value1=20
value2=8000 #bin size
value3=1 #window size
value4="ESC4DN_merge_DI250" #name prefix 

region=$(($value2*$value3))


for ((i=$value0;i<=$value1;i++));
do
    awk -v awk_val=chr$i -F'\t' '$1 == awk_val {print $0}' ../output/$value4\_hmm_7colfile_c > ../output/chr$i\hmmfile2

    awk -v awk_val=$i -F'\t' '{if (NR > 1) {if (($1 == awk_val) && ($1 == prevchr)) { if ($2 != prev) {printf("%d\t%d\n", prev, $2)}}};prev = $3;prevchr = $1}' ../output/$value4\_postp_list.csv > ../output/gaps$i
    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2016-12_geschwind40kb_chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2017_4_rao50kb_chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2018_10_4dnucleome8kb_chromend.txt chr${i} | perl hmm-state_domains.pl  | perl final_check.pl ../output/gaps$i > ../output/$value4\_finaldomains_chr${i}

    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2018_1_cavalli10kb__chromend.txt chr${i} | perl hmm-state_domains.pl  | perl final_check.pl ../output/gaps$i > ../output/$value4\_finaldomains_chr${i}

    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2016-12_geschwindgz40kb_chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    rm ../output/chr${i}\hmmfile2
    rm ../output/gaps*

done

