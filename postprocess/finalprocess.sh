#! /bin/bash
# inputfile must be bed file plus corresponding DI and hidden state values

#Quis custodiet ipsos custodes?


value0=1 #start chr
value1=20 #stop chr
value2=10000 #bin size
value3=1 #window size
value4="blobel_60min_10000v3_merge_DI300" #name prefix 

region=$(($value2*$value3))


for ((i=$value0;i<=$value1;i++));
do
    awk -v awk_val=chr$i -F'\t' '$1 == awk_val {print $0}' ../output/$value4\_hmm_7colfile_c > ../output/chr$i\hmmfile2
 
    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2018_2_mm10_10kb__chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2017_4_rao50kb_chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2017_5_dixon2015ES.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2016-12_geschwindgz40kb_chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    #perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2017_akh_20kb_chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    perl hmm_probablity_correcter.pl ../output/chr${i}\hmmfile2 2 0.99 $region | perl hmm-state_caller.pl ../output/2018_3_blobelmitosis10kb_chromend.txt chr${i} | perl hmm-state_domains.pl > ../output/$value4\_finaldomains_chr${i}

    rm ../output/chr${i}\hmmfile2

done

