#! /bin/bash
#
#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/ES/ES_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 200 301 263909 1 20 10000 1 0 False ES_Cav_DI200

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/NPC/NPC_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 200 301 263909 1 20 10000 1 0 False NPC_Cav_DI200

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/CN/CN_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 200 301 263909 1 20 10000 1 0 False NP_Cav_DI200

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/ES/ES_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 300 301 263909 1 20 10000 1 0 False ES_Cav_DI300

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/NPC/NPC_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 300 301 263909 1 20 10000 1 0 False NPC_Cav_DI300

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/CN/CN_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 300 301 263909 1 20 10000 1 0 False NP_Cav_DI300

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/ES/ES_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 400 301 263909 1 20 10000 1 0 False ES_Cav_DI400

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/NPC/NPC_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 400 301 263909 1 20 10000 1 0 False NPC_Cav_DI400

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/CN/CN_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 400 301 263909 1 20 10000 1 0 False NP_Cav_DI400

python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/hic_data/counts/ES_mm10_genomewide.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/hic_data/counts/cavalli_mm10_genomewide.bed 300 301 263173 1 20 10000 1 0 False ES_mm10_Cav_DI300

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/hic_data/counts/NPC_mm10_genomewide.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/hic_data/counts/cavalli_mm10_genomewide.bed 300 301 263173 1 20 10000 1 0 False NPC_mm10_Cav_DI300

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/hic_data/counts/CN_mm10_genomewide.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/hic_data/counts/cavalli_mm10_genomewide.bed 300 301 263173 1 20 10000 1 0 False CN_mm10_Cav_DI300
