#! /bin/bash
#


bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 30 79 386046 1 24 8000 1 0 False 1 HFF4DN_merge_DI30

bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 40 79 386046 1 24 8000 1 0 False 1 HFF4DN_merge_DI40

bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 150 79 386046 1 24 8000 1 0 False 1 HFF4DN_merge_DI150

#bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 50 79 386046 1 24 8000 1 0 False 1 HFF4DN_merge_DI50

#bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 75 79 386046 1 24 8000 1 0 False 1 HFF4DN_merge_DI75

#bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 100 79 386046 1 24 8000 1 0 False 1 HFF4DN_merge_DI100

#bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 250 79 386046 1 24 8000 1 0 False 1 HFF4DN_merge_DI250


#bsub -o 4dnucleome_HFF.o -e 4dnucleome_HFF.e python DI_TAD_calls2Hermitian.py input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.counts input/U54-HFFc6-p17-p22-FA-DpnII-20170327_hg38.1000.balanced.multires_genomewide_8kb.bed 50 79 386046 1 24 8000 1 0 False 2 HFF4DN_merge_DI50adjust2
