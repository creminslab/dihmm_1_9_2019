#! /bin/bash
#
#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/ES/ES_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 100 301 263909 1 20 10000 1 0 False ES_Cav_DI100

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/NPC/NPC_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 100 301 263909 1 20 10000 1 0 False NPC_Cav_DI100

#python DI_TAD_calls2Hermitian.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/CN/CN_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 100 301 263909 1 20 10000 1 0 False NP_Cav_DI100

#python DI_TAD_calls2Hermitianmod.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/ES/ES_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 50 301 263909 1 20 10000 1 0 False ES_Cavmod_DI50 

#python DI_TAD_calls2Hermitianmod.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/NPC/NPC_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 50 301 263909 1 20 10000 1 0 False NPC_Cavmod_DI50

#python DI_TAD_calls2Hermitianmod.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/CN/CN_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 50 301 263909 1 20 10000 1 0 False NP_Cavmod_DI50

#using ~/bashrc_heidi
python DI_TAD_calls2Hermitianlog.py /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/ES/ES_cavalli_genome.counts /project/jcreminslab/hharvey_projects/netmet_repo1/input/cavalli/cavalli_hiC_10000_abs.bed 50 301 263909 1 20 10000 1 0 False ES_Cavlog_DI50
