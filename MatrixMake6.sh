#! /bin/bash
#
# Script file to making one MatrixMake Output
# MatrixMake6.py counts bed start_bin stop_bin Heatmap_no name_in_output

#python MatrixMake6.py /project/jcreminslab/Data/project013_RencortexHiC/output_cortex_all_lanes_07_02_2016/hic_results/matrix/cortex_all_lanes/iced/40000/cortex_all_lanes_40000_iced.matrix /project/jcreminslab/Data/project013_RencortexHiC/output_cortex_all_lanes_07_02_2016/hic_results/matrix/cortex_all_lanes/raw/40000/cortex_all_lanes_40000_abs.bed 9550 9575 9550 9575 5 HiCRen_test

# Script file to make lots of batches of MatrixMake Outputs
start=303
end=1000
i=1
counts="/project/jcreminslab/Data/project012_BlobelBrd2HiC/output_Hsu_HiC_Brd2KO_D_all5mergedfiles_1_07_2017_upperice10kb1000iterationstop1bottom10complete/hic_results/matrix/Brd2KO_D_all5plusmergedreps/iced/10000/Brd2KO_D_all5plusmergedreps_10000_iced.matrix"
bed="/project/jcreminslab/Data/project012_BlobelBrd2HiC/output_Hsu_HiC_WT_JC4_all5mergedfiles_1_07_2017_upperice10kb1000iterationstop1bottom10complete/hic_results/matrix/WT_JC4_all5plusmergedreps/raw/10000/WT_JC4_all5plusmergedreps_10000_abs.bed"

python MatrixMake6.py $counts $bed $start $end $start $end $i Brd2KO_Dchr1


