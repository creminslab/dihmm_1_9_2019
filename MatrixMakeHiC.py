# MatrixMakeBanded
# version 6 by Daniel Emerson demerson368@gmail.com
# Jan 2 2016
#update to MatrixMake4.py (uses same input style as MatrixMake4.py - no strings, same code as MatrixMake5.py but different input format)

#simpler design than MatrixMakeBanded3.py - makes a zero numpy array where non-zero counts are added
#on top of instead of finding gaps between counts.  Also, much faster

#Program is designed to give a banded heatmap around diagonal value (organized as diagonal+- range)
#so sequence of 1 2 3 4 5 6 7 8 9 10 with range +- 2 gives:
#
#    1 2 3 4 5
#    2 3 4 5 6
#    3 4 5 6 7
#    4 5 6 7 8
#    5 6 7 8 9
#    6 7 8 9 10

# first and last 2 of sequence get cut off (no room for range)
# windowing handled by convertbed2.py after DI8.py is run

# python MatrixMake4.py pcount startindex endindex range heatmapcopy# name

import argparse
import os
import sys
import re
import random
import copy
import csv
import numpy as np

class MatrixMake:
      Maximum = 0
      Minimum = 100000
      stop_processing = False

      def __init__(self, filename,filename2,start,stop,first,last,region,number):
           self.filename = filename  #input counts
           self.filename2 = filename2
           self.start = int(start)
           self.stop = int(stop)
           self.first = int(first)
           self.last = int(last)
           self.number = str(number)
           self.pcount = []
           self.list_of_bins = []
           self.list_of_regions = []

           for i in range(int(self.start),int(self.stop)+1):
               self.list_of_bins.append(i)
           
           MatrixMake.Maximum = int(self.last)
           MatrixMake.Minimum = int(self.first)
           
           self.Heatmap = np.zeros(((self.stop-self.start)+1,(self.last-self.first)+1))
           self.region = region
           self.readfile()
           self.pCountMake()


      def readfile(self):

           f = open(self.filename,"r")
           for lines in f:
                lines = lines.strip('\n')
                values = lines.split('\t')
                values = [i.strip(' ') for i in values]
                self.test(values)
                if MatrixMake.stop_processing:  #keep from wasting too much time processing lines if exceeds the end
                     break
           f.close()

           f = open(self.filename2,"r")
           processing = 1
           for lines in f:
               lines = lines.strip('\n')
               values = lines.split('\t')
               values = [i.strip(' ') for i in values]
               self.test2(values,processing)
           #    if MatrixMake.stop_processing:  #keep from wasting too much time processing lines if exceeds the end
           #         break
           f.close()



      def test(self,values):
           #test to see if input line from counts is suitable for storage
           firstcol = values[0]
           secondcol = values[1]
           #print "int(firstcol[2]) = ", int(firstcol[2]) 
           #print "self.start = ", self.start
           #print "self.stop = ", self.stop 

           if ((int(firstcol) >= self.start) and (int(firstcol) <= self.stop)):
                #print "found value"
                if ((int(secondcol) >= self.first) and (int(secondcol) <= self.last)):
                       bin1 = int(firstcol) - self.start
                       bin2 = int(secondcol) - self.first
                       self.Heatmap[bin1][bin2] = float(values[2])

           if int(firstcol) > self.stop:
                #print "stopping"
                MatrixMake.stop_processing = True

      def test2(self,values,processing):
           forthcol = values[3]
           binstart = int(forthcol) - self.start
           digits = len(str(MatrixMake.Maximum))
           print self.list_of_bins
           print int(forthcol)

           if (int(forthcol) in self.list_of_bins):
               print "in list"
               self.list_of_regions.append([values[0],values[1],values[2],self.region + self.number + "_BIN_" + str(binstart).zfill(digits)])  #attach all rows of .bed input to matching bins
           #    processing = processing + 1
           #if (processing > (int(self.stop) - int(self.start) + 2)):  # already found all region matchups no longer need to search file
           #    MatrixMake.stop_processing

      def pCountMake(self):
          #store pcount data

          digits = len(str(MatrixMake.Maximum))
          temppcount =  []
          b = self.start + 1
          #print "len(self.Heatmap) = ",len(self.Heatmap)
          for i in range(0,len(self.Heatmap)):
               for a in range(self.start,b):
                   #print "self.Heatmap[i][2] = ", self.Heatmap[i][2]
                   self.pcount.append([self.region + self.number + "_BIN_" + str(i).zfill(digits),self.region + self.number + "_BIN_" + str(a-self.start).zfill(digits),str(self.Heatmap[i][a-self.start]).strip(" ")])
               b = b + 1


      def save(self,fileName,fileName2,fileName3):
          np.savetxt(fileName, self.Heatmap, delimiter=',',fmt='%f')
          
          f = open(fileName2,"w") # pCount
          f.write('\n'.join('\t'.join('{:1}'.format(item) for item in row) for row in self.pcount))
          f.write('\n')
          f.close();

          f = open(fileName3,"w") # Bed
          f.write('\n'.join('\t'.join('{:1}'.format(item) for item in row) for row in self.list_of_regions))
          f.write('\n')
          f.close();
          


def REPL():


      parser = argparse.ArgumentParser()
      parser.add_argument("count", type=str, help="count file")
      parser.add_argument("bed", type=str, help="bed file")
      parser.add_argument("start", type=int, help="chosen start")
      parser.add_argument("stop", type=int, help="chosen end")
      parser.add_argument("first", type=int, help="actual first bin number")
      parser.add_argument("last", type=int, help="actual last bin number")
      parser.add_argument("number", type=str, help ="copy of heatmap")
      parser.add_argument('region', nargs='?', default="regionX")
      args = parser.parse_args()

      data = MatrixMake(args.count,args.bed,args.start,args.stop,args.first,args.last,args.region,args.number.zfill(3))
      #data.save(args.region + args.number.zfill(3) + "_bandheatmap.csv")
      data.save(args.region + args.number.zfill(3) + "_heatmap.csv",args.region + args.number.zfill(3) + "_pvalues.counts",args.region + args.number.zfill(3) +".bed")


if __name__ == "__main__":
    REPL()
