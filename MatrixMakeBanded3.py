# MatrixMakeBanded
# version 3 by Daniel Emerson demerson368@gmail.com
# Sept 3 2016

#Program is designed to give a banded heatmap around diagonal value (organized as diagonal+- range)
#so sequence of 1 2 3 4 5 6 7 8 9 10 with range +- 2 gives:
#
#    1 2 3 4 5
#    2 3 4 5 6
#    3 4 5 6 7
#    4 5 6 7 8
#    5 6 7 8 9
#    6 7 8 9 10

# first and last 2 of sequence get cut off (no room for range)

# python MatrixMake3.py pcount startindex endindex range heatmapcopy# name

import argparse
import os
import sys
import re
import random
import copy
import csv


class MatrixMakeBanded:
      Maximum = 0
      Minimum = 100000
      stop_processing = False

      def __init__(self, filename,start,end,distance,region):
           self.Heatmap = []  # matrix ((end-start)+1) x (range + 1)
           self.myList = []
           self.filename = filename  #input counts
           self.start = int(start)
           self.end = int(end)
           self.distance = int(distance)
           self.list_of_bins = []
           self.list_of_regions = []
           self.region = region

           self.readfile()
           self.Btruncate()
           self.buffer()
           self.truncate()
           self.Elongate()
           self.postProcess()
           #self.postProcess2()
           #self.Btruncate()
           self.HM()


      def readfile(self):

           f = open(self.filename,"r")
           for lines in f:
                lines = lines.strip('\n')
                values = lines.split('\t')
                values = [float(i.strip(' ')) for i in values]
                self.test(values)
                MatrixMakeBanded.Maximum = int(self.end)
                MatrixMakeBanded.Minimum = int(self.start)
                if MatrixMakeBanded.stop_processing:  #keep from wasting too much time processing lines if exceeds the end
                     break
           f.close()



      def test(self,values):
           #test to see if input line from counts is suitable for storage

           if ((abs(int(values[0]- values[1])) <= int(self.distance))): #distance between itself and neighbor
                self.myList.append(values)  #store bins that are found in square matrix region
                print "adding bin = ", int(values[0])
           else:
                if (int(values[0]) > int(self.end)):
                    MatrixMakeBanded.stop_processing = True

      def Btruncate(self): #initial removal of rows where there is not enough room for band upstream/downstream
           print("truncating beginning/end ")
           print("myList.size() = ", len(self.myList))
           j = 1
           while ((j == 1) and (len(self.myList) != 0)):  #remove first segments if not enough range
                print("truncating beginning: myList.size() = ", len(self.myList))
                if ((self.myList[0][0] - MatrixMakeBanded.Minimum) - self.distance  < 0):
                     self.myList.pop(0)
                else:
                     break
           j = 1
           while ((j == 1) and (len(self.myList) != 0)):  #remove end segments if not enough range
                print("truncating end: myList = ", len(self.myList))
                if ((MatrixMakeBanded.Maximum  - self.myList[len(self.myList)-1][0]) - self.distance < 0):
                     self.myList.pop(len(self.myList)-1)
                else:
                     break
 

      def buffer(self):

          i = 0
          difference = self.myList[0][0] - self.myList[0][1]
          if ((abs(difference) < self.distance) and (abs(difference) >= 0)):
               gapSize = int(self.distance - difference)
               neighbor = self.myList[i][1]-gapSize
               for j in range(0,gapSize):
                    valueInsert = [self.myList[i][0],neighbor,"         0"]
                    self.myList.insert(i,valueInsert)
                    i = i + 1
                    neighbor = neighbor + 1
          i = i + 1 # want to start at second value unbuffered, which is i+ 1 buffered
          while (i < len(self.myList)):
               # Buffer below center (if necessary)
               # if neighbor is smaller than range away from center
               # if previous row was also from same segment
               # if previous neighbor of previous row was greater than range away
               # fill in zeros between rows to give 0 interaction at end range
               #
               difference = self.myList[i][0] - self.myList[i][1]
               if ((abs(difference) < self.distance) and (abs(difference) >= 0)):
                    if (self.myList[i-1][0] == self.myList[i][0]): #finding range within segment
                         if ((self.myList[i][0] - self.myList[i-1][1]) > self.distance):
                              print("Buffering: Adding 0s, myList size = ", len(self.myList))
                              neighbor = self.myList[i-1][1]+1
                              gapSize = int(abs(self.myList[i][1] - self.myList[i-1][1]))
                              for j in range(0,gapSize):
                                   valueInsert = [self.myList[i][0],neighbor,"         0"]
                                   self.myList.insert(i,valueInsert)
                                   i = i + 1
                                   neighbor = neighbor + 1
                    else:
                         print("Buffering: Adding 0s, at edge() ", len(self.myList))
                         gapSize = int(self.distance - difference)  #finding range at edge
                         neighbor = self.myList[i][1]-gapSize
                         for j in range(0,gapSize):
                              valueInsert = [self.myList[i][0],neighbor,"         0"]
                              self.myList.insert(i,valueInsert)
                              i = i + 1
                              neighbor = neighbor + 1
               i=i+1
          i=0
          while (i < len(self.myList)-1):
               # Buffer above center (if necessary)
               # if neighbor is smaller than range away from center
               # if previous row was also from same segment
               # if previous neighbor of previous row was greater than range away
               # fill in zeros between rows to give 0 interaction at end range
               #
               difference = self.myList[i][1] - self.myList[i][0]
               if ((abs(difference)  < self.distance) and (abs(difference)  >= 0)):
                    if (self.myList[i+1][0] == self.myList[i][0]):  #finding range within segment
                         if ((self.myList[i+1][1] - self.myList[i][0]) > self.distance):
                              print("Buffering: Adding 0s, myList.size() = ", len(self.myList))
                              neighbor = self.myList[i][1]+1
                              gapSize = int(abs(self.myList[i+1][1] - self.myList[i][1]))
                              for j in range(0,gapSize):
                                   valueInsert = [self.myList[i][0],neighbor,"         0"]
                                   self.myList.insert(i+1,valueInsert)
                                   i = i + 1
                                   neighbor = neighbor + 1
                    else:
                         print("Buffering: Adding 0s, at edge()", len(self.myList))
                         gapSize = int(abs(self.distance - difference))  #finding range at edge
                         neighbor = self.myList[i][1] + 1
                         for j in range(0, gapSize):
                               valueInsert = [self.myList[i][0],neighbor,"         0"]
                               self.myList.insert(i+1,valueInsert)
                               i = i + 1
                               neighbor = neighbor + 1

               i = i + 1
          # check end of file
          difference = self.myList[len(self.myList)-1][1] - self.myList[len(self.myList)-1][0]
          if ((abs(difference)  < self.distance) and (abs(difference)  >= 0)):
               gapSize = int(self.distance - difference)  #finding range at edge
               neighbor = self.myList[len(self.myList)-1][1] + 1
               for j in range(0,gapSize):
                    valueInsert = [self.myList[len(self.myList)-1][0],neighbor,"         0"]
                    self.myList.append(valueInsert)
                    neighbor = neighbor + 1

      def truncate(self):
           j = 0;
           while (j < len(self.myList)):  #remove out of range values for all
                print("truncating: myList.size() = ", len(self.myList))
                if (abs(self.myList[j][0] - self.myList[j][1]) > self.distance):
                     self.myList.remove(self.myList[j])
                     j = j - 1
                j = j + 1
      
      def Elongate(self):  #add 0s as filler within range
           i = 1
           while (i < len(self.myList)):
                if (self.myList[i][0] == self.myList[i-1][0]):
                     if (self.myList[i][1] - self.myList[i-1][1] != 1):
                          neighbor = self.myList[i-1][1]+1
                          gapSize = int(abs(self.myList[i][1] - self.myList[i-1][1])-1)
                          for j in range(0,gapSize):
                               valueInsert = [self.myList[i][0],neighbor,"         0"]
                               self.myList.insert(i,valueInsert)
                               i = i + 1
                               neighbor = neighbor + 1
                i = i + 1




      def postProcess(self):  #necessary for possible missing segments (1 column skips over bins)

           i = 0
           if (self.myList[0][0] != (int(self.start)+int(self.distance))):  #start bin region does not have +- range
                gapSize = int(self.myList[0][0] -(int(self.start) + int(self.distance)))
                if (gapSize > 1):  
                     segment = int(self.start)+int(self.distance)
                     for i in range(0,gapSize):
                          neighbor = segment - self.distance
                          for k in range(0, 2*self.distance+1):
                               valueInsert = [segment,neighbor,"         0"]
                               self.myList.insert(i,valueInsert)
                               neighbor = neighbor + 1
                               i = i+1
                          segment = segment + 1

           i = 0
           while (i < len(self.myList)-2):
                print("post elongate iteration ", i, " size ", len(self.myList))
                if (i>0):
                     if (self.myList[i][0] != self.myList[i-1][0]) and (self.myList[i][0] != self.myList[i+1][0]):
                          i = self.postProcess2(i)
                gapSize = int(self.myList[i+1][0] - self.myList[i][0])
                if (gapSize > 1):  #if column skips over segment (presumably all 0s)
                     for j in range(1,gapSize):  #fill in for however large gap is
                          segment = self.myList[i][0]+1
                          neighbor = segment - self.distance
                          for k in range(0, 2*self.distance+1):
                               valueInsert = [segment,neighbor,"         0"]
                               self.myList.insert(i+1,valueInsert)
                               neighbor = neighbor + 1
                               i = i+1
                i = i + 1

           if ((int(self.end) - int(self.distance)) != self.myList[len(self.myList) -1][0]): #end bin does not have +- range
                add_to_end = self.myList[len(self.myList) -1][0]
                gapSize = (int(self.end) - int(self.distance)) - int(add_to_end)
                segment = add_to_end + 1
                for i in range(0,gapSize):
                     neighbor = segment - self.distance
                     for k in range(0, 2*self.distance+1):
                          valueInsert = [segment,neighbor,"         0"]
                          self.myList.append(valueInsert)
                          neighbor = neighbor + 1
                     segment = segment + 1
      
      def postProcess2(self,i): #run one more time through
           if (self.myList[i][1] > self.myList[i][0]):
                piece = self.myList[i][1]-self.myList[i][0]
                gapSize = int(int(self.distance)+piece)
                neighbor = self.myList[i][0] - int(self.distance)
                for j in range(0,gapSize):
                     valueInsert = [self.myList[i][0],neighbor,"         0"]
                     self.myList.insert(i,valueInsert)
                     i = i + 1
                     neighbor = neighbor + 1
                if piece < int(self.distance): # need to add above
                     i = i + 1 #skip over single value
                     gapSize = int(int(self.distance) - piece)
                     neighbor = self.myList[i][1] + 1
                     for j in range(0,gapSize):
                          valueInsert = [self.myList[i][0],neighbor,"         0"]
                          self.myList.insert(i,valueInsert)
                          i = i + 1
                          neighbor = neighbor + 1
           elif (self.myList[i][1] < self.myList[i][0]):
                piece = self.myList[i][0] - self.myList[i][1]
                gapSize = int(int(self.distance)-piece)
                if gapSize > 0:
                     neighbor = self.myList[i][0] - int(self.distance)
                     for j in range(0,gapSize):
                          valueInsert = [self.myList[i][0],neighbor,"         0"]
                          self.myList.insert(i,valueInsert)
                          i = i + 1
                          neighbor = neighbor + 1
                gapSize = int(int(self.distance)+piece)
                neighbor = self.myList[i][0] + 1
                i = i + 1 #skip of single value to add above
                for j in range(0,gapSize):
                     valueInsert = [self.myList[i][0],neighbor,"         0"]
                     self.myList.insert(i,valueInsert)
                     i = i+ 1
                     neighbor = neighbor + 1
           else:
                neighbor = self.myList[i][0] - int(self.distance)
                for j in range(0,int(self.distance)):
                     valueInsert = [self.myList[i][0],neighbor,"         0"]
                     self.myList.insert(i,valueInsert)
                     i = i + 1
                     neighbor = neighbor + 1
                neighbor = self.myList[i][0] + 1
                i = i + 1 # skip over single value
                for j in range(0,int(self.distance)):
                     valueInsert = [self.myList[i][0],neighbor,"         0"]
                     self.myList.insert(i,valueInsert)
                     i = i + 1
                     neighbor = neighbor + 1

           return i

      def HM(self):  #make final banded HeatMap
           increment = 1
           row = []
           compare = []
           for i in range(0,len(self.myList)):
                if (increment == 1):
                     row.append(self.myList[i][2])
                     compare.append(self.myList[i][0])
                     increment = increment + 1
                else:
                     if (i < len(self.myList)-1):
                          if (compare[len(compare)-1] == self.myList[i][0]):
                               row.append(self.myList[i][2])
                               compare.append(self.myList[i][0])
                               increment = increment + 1
                          else:
                               self.Heatmap.append(row)
                               compare.append(self.myList[i][0])
                               row = []
                               row.append(self.myList[i][2])
                               increment = increment + 1
                     else:
                          row.append(self.myList[i][2]) #add final value of myList2 to last row
                          self.Heatmap.append(row)


      def save(self,fileName):
          #make output files

          f = open(fileName,"w") # Heatmap (end - start) +1 x (end - start) + 1
          f.write('\n'.join(','.join('{:10}'.format(item) for item in row) for row in self.Heatmap))
          f.close();



def REPL():


      parser = argparse.ArgumentParser()
      parser.add_argument("count", type=str, help="count file")
      parser.add_argument("start", type=int, help="start")
      parser.add_argument("end", type=int, help="end")
      parser.add_argument("range", type=int, help="upstream/downstream range")
      parser.add_argument("number", type=str, help ="copy of heatmap")
      parser.add_argument('region', nargs='?', default="regionX")
      args = parser.parse_args()

      data = MatrixMakeBanded(args.count,args.start,args.end,args.range,args.region)
      data.save(args.region + args.number.zfill(3) + "_bandheatmap.csv")



if __name__ == "__main__":
    REPL()
